#include "forest.h"

/*Allocate memory for a new Forest containing a single tree*/
Forest * forest_create(Node * tree) {
	Forest f;
	f.trees = list_create_list(tree);
	Forest * res = malloc(sizeof(Forest));
	*res = f;
	return res;
}

/*Free the memory allocated for a forest along with all node it containe*/
void forest_destroy(Forest * forest) {
	list_destroy_with_function(forest->trees, (ListFunctionDestroy) tree_free_all);
	free(forest);
}

/*Add a tree to the forest*/
void forest_add_tree(Forest * forest, Node * tree) {
	list_add_data_end(forest->trees, tree);
}

/*Remove a tree from the forest. That tree pointer must be the same inside the forest*/
void forest_remove_tree(Forest * forest, Node * tree) {
	list_remove_data(&(forest->trees), tree);
}

/*Add a tree node to the forest.
The argument <father> is the node under which to add the new node,
If its NULL, the node is add as a new tree.*/
/*Ajoute un node (node) comme fils d'un autre (pere).
(Se refere uniquement au tag contenu dans pere)
Si pere n'est pas dans la la forest ou est null, le node est ajouter comme tree*/
void forest_add_node(Forest * forest, Node * node, Node * father) {
	if (father == NULL) {
		forest_add_tree(forest, node);
	}
	
	Node * vraiPere = forest_is_tag_in(forest, father->tag);
	if(vraiPere == NULL) {
		forest_add_tree(forest, node);
	}
	else {
		tree_node_add_fils(vraiPere, node);
	}
}

static void * forest_delete_tag_on_tree(Node * tree, void ** arg) {
	Node * node = (Node*) arg[0];
	Forest * forest = (Forest*) arg[1];
	int res = tree_delete_node(tree, node->tag, NULL);
	if(res == 1) {
		forest_remove_tree(forest, tree);
		return tree;
	}
	return NULL;
}
/*Supprime un node de la forest*/
void forest_delete_tag(Forest * forest, Node * node) {
	void * arg[] = {node, forest};
	list_browse_return(forest->trees, (ListFunctionCheck )forest_delete_tag_on_tree, (void*)arg);
}

static void forest_browse_on_node(Node * node, Node * pere, void * allarg) {
	void ** arg = (void **)allarg;
	ForestFunction callback = (ForestFunction) arg[0];
	Node * tree = (Node *)arg[1];
	void * other = arg[2];

	callback(tree, node, pere, other);
}
static void forest_browse_on_tree(Node * tree, void * allarg) {
	void ** arg = (void **)allarg;
	ForestFunction callback = (ForestFunction) arg[0];
	void * other = arg[1];

	void * narg[] = {callback, tree, other};
	tree_parcours_profondeur(tree, (TreeFunction)forest_browse_on_node, (void *)narg);
}
/*Parcours la forest tree par tree.
Pour chaque node de chaque tree, callback est appelle avec ce node, son tree correspondant et la donnee other*/
void forest_browse(Forest * forest, ForestFunction callback, void * other) {
	void * arg[] = {callback, other};
	list_browse(forest->trees, (ListFunction)forest_browse_on_tree, (void *)arg);
}

static void * forest_is_tag_in_on_tree(Node * tree, const char * tag) {
	return tree_find_node(tree, tag);
}
/*Teste si un tag est deja dans une forest*/
Node * forest_is_tag_in(Forest * forest, void * tag) {
	Node * target = list_browse_return(forest->trees, (ListFunctionCheck)forest_is_tag_in_on_tree, (void*)tag);
	return target;
}
