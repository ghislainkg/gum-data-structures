#ifndef _GUM_LIST_
#define _GUM_LIST_

#include <stdlib.h>

#define TRUE 1
#define FALSE 0

typedef struct List {
	void * data;
	struct List * next;
} List;

// Type de fonctions appellees lors du parcours de la liste
typedef void (*ListFunction)(void * data, void * other);
// Type de fonctions appellees lors de la recherche, elle teste si on a trouver l'element recherche
typedef void * (*ListFunctionCheck)(void * data, void * other);

typedef int (*ListFunctionWhen)(void * data, void * other);

typedef void (*ListFunctionDestroy)(void * data);

typedef void * (*ListFunctionCopy)(void * data);

/* Creation d'une liste contenant un seul element*/
List * list_create_list(void * data);

/*Fait une copy de la List*/
List * list_copy(List * list, ListFunctionCopy copy);

/* Detruit tous les elements de la liste
Ne detruit pas les data*/
void list_destroy(List * list);

/* Detruit tous les elements de la liste
Detruit aussi les data*/
void list_destroy_free_data(List * list);

/* Detruit tous les elements de la liste en utilisant <destroyFunction> pour detruire les data*/
void list_destroy_with_function(List * list, ListFunctionDestroy destroyFunction);

/* Ajout d'un element a une liste*/
void list_add_data_end(List * list, void * data);

/*Suppression d'un element d'une liste.
L'element dont le pointeur data est le meme que le data passe en parametre est supprime
Si la liste ne contient qu'un element, elle est mise a NULL*/
void list_remove_data(List ** list, void * data);

/*Suppression d'un element d'une liste. Retourne 1 si quelque chose a ete supprime et 0 sinon
Suppression d'un element de la liste pour lequel when retourne TRUE
Si la liste ne contient qu'un element, elle est mise a NULL*/
int list_remove_data_when(List ** list, ListFunctionWhen when, void * other);

/* Parcours d'une liste*/
void list_browse(List * list, ListFunction callBack, void * other);

/* Retourne le premier retour de callBack qui n'est pas NULL
callBack est appeller avec chaque element de la liste jusqu'a ce qu'il retourne une valeur non NULL*/
void * list_browse_return(List * list, ListFunctionCheck callBack, void * other);

#endif