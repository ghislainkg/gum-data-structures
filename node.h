#ifndef _GUM_NODE_
#define _GUM_NODE_

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "list.h"

typedef struct Node {
	void * tag;
	unsigned int taglen;
	struct Node * pere;
	List * fils;
	unsigned int filsCount;
} Node;

typedef void (*TreeFunction) (Node * node, Node * pere, void * other);
typedef void (*TreeFunctionProfondeur) (Node * node, Node * pere, int profondeur, void * other);

/*Creation d'un node sans pere ni fils*/
Node * tree_create_node(void * tag);

/*Libere la memoire aloue pour le node <node> et tout ses descendants*/
void tree_free_all(Node * node);

/*Ajout d'un fils a un node*/
void tree_node_add_fils(Node * node, Node * fils);

/*Retourne le Node de tag "tag" dans l'arborescence de "start"*/
Node * tree_find_node(Node * start, void * tag);

/*Fait un parcours en profondeur de l'arborescence de start.
pour chaque node, callback est appelle avec le node et son pere*/
void tree_parcours_profondeur(Node * start, TreeFunction callback, void * other);

/*Supprime de l'arbre tous les nodes de tag <tag> et tout leurs fils
Si start est le noeud de tag <tag>, on retourne 1. si non on retourne toujours 0.
Le node est enleve de l'arbre mais pas supprime, il est retourne dans <deletedNode>*/
int tree_delete_node(Node * start, void * tag, Node ** deletedNode);

/*Retourne une liste des nodes au dessus du node <node> et lui meme*/
List * tree_get_peres(Node * node);

/*Retourne une list des descendants du node*/
List * tree_get_descendants(Node * node);

/*Fait un parcours en largeur de l'arborescence de start.
pour chaque node, callback est appelle avec le node et son pere*/
void tree_parcours_largeur(Node * start, TreeFunction callback, void * other);


/*Affiche l'arbre <arbre>*/
void tree_display_arbre(Node * arbre);

/*Fait un parcours en profondeur de l'arborescence de start.
pour chaque node, callback est appelle avec le node, son pere et sa profondeur dansl'arbre start*/
void tree_parcours_profondeur_with_profondeur_aux(Node * start, TreeFunctionProfondeur callback, int * indent, Node ** prevPere, void * other);
/*Fait un parcours en profondeur de l'arborescence de start.
pour chaque node, callback est appelle avec le node, son pere et sa profondeur dansl'arbre start*/
void tree_parcours_profondeur_with_profondeur(Node * start, TreeFunctionProfondeur callback, void * other);

/*Transforme une List de Node en une liste de char correspondant aux tags des Node*/
List * tree_node_list_to_tag_list(List * nodes);

#endif