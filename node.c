#include "node.h"

/*Creation d'un node sans father ni fils*/
Node * tree_create_node(void * tag, unsigned int taglen) {
	Node node;
	node.tag = malloc(taglen);
	memmove(node.tag, tag, taglen);

	node.father = NULL;
	node.sons = NULL;
	node.sonsCount = 0;

	Node * res = malloc(sizeof(Node));
	*res = node;
	return res;
}

static void tree_free_all_on_node(Node * node, void * other) {
	list_destroy(node->sons);
	tree_delete_node(node->father, node->tag, NULL);
	free(node->tag);
	free(node);
}
/*Libere la memoire aloue pour le node <node> et tout ses descendants*/
void tree_free_all(Node * node) {
	tree_parcours_largeur(node, (TreeFunction)tree_free_all_on_node, NULL);

	free(node->tag);
	list_destroy(node->sons);
	free(node);
}

/*Ajout d'un fils a un node*/
void tree_node_add_fils(Node * node, Node * fils) {
	if(node->sonsCount == 0) {
		// Si on n'a pas encore de liste de fils,
		// On fixe une nouvelle liste contenant le fils
		node->sons = list_create_list(fils);
	}
	else {
		list_add_data_end(node->sons, fils);
	}
	fils->father = node;
	node->sonsCount ++;
}

static Node * tree_find_node_on_node(Node * node, const char * tag) {
	unsigned int len = strlen(tag);
	if(len == strlen(node->tag) && memcmp(node->tag, tag, len) == 0) {
		return node;
	}
	else  {
		return tree_find_node(node, tag);
	}
}
/*Retourne le Node de tag "tag" dans l'arborescence de "start"*/
Node * tree_find_node(Node * start, void * tag) {
	if(strlen(tag) == strlen(start->tag) && memcmp(start->tag, tag, strlen(tag)) == 0) {
		return start;
	}
	return list_browse_return(start->sons, (ListFunctionCheck)tree_find_node_on_node, (void*)tag);
}

static void tree_parcours_profondeur_on_fils(Node * f, void * allargs) {
	// On a un fils
	// Depaquettage du paquet
	void ** arg = (void**)allargs;
	TreeFunction callback = (TreeFunction) arg[0];
	Node * father = (Node*) arg[1];
	void * other = arg[2];
	
	// Petit warning pour signaler une incoherance dans l'arbre
	// (Petit truc de parano)
	if(father != NULL && father != f->father) {
		printf("Probleme dans l'arbre : Le fils ne connais pas son vrai father et son father le connais");
	}
	
	// Appel du callback
	callback(f, father, other);

	// Maintenant le fils devient un father, et on continu
	tree_parcours_profondeur(f, callback, other);
}
/*Fait un parcours en profondeur de l'arborescence de start.
pour chaque node, callback est appelle avec le node et son father*/
void tree_parcours_profondeur(Node * start, TreeFunction callback, void * other) {
	// Empacketage du callback et du father pour le parcours des fils de ce dernier
	void * arg[] = {callback, start, other};
	list_browse(start->sons, (ListFunction)tree_parcours_profondeur_on_fils, (void*)arg);
}

static void tree_parcours_largeur_on_fils(Node * f, void * allargs) {
	// On a un fils
	// Depaquettage du paquet
	void ** arg = (void**)allargs;
	TreeFunction callback = (TreeFunction) arg[0];
	Node * father = (Node*) arg[1];
	void * other = arg[2];
	
	// Petit warning pour signaler une incoherance dans l'arbre
	// (Petit truc de parano)
	if(father != NULL && father != f->father) {
		printf("Probleme dans l'arbre : Le fils ne connais pas son vrai father et son father le connais");
	}
	// Maintenant le fils devient un father, et on continu
	tree_parcours_largeur(f, callback, other);

	// Appel du callback
	callback(f, father, other);
}
/*Fait un parcours en largeur de l'arborescence de start.
pour chaque node, callback est appelle avec le node et son father*/
void tree_parcours_largeur(Node * start, TreeFunction callback, void * other) {
	// Empacketage du callback et du father pour le parcours des fils de ce dernier
	void * arg[] = {callback, start, other};
	list_browse(start->sons, (ListFunction)tree_parcours_largeur_on_fils, (void*)arg);
}

/*Supprime de l'arbre tous les nodes de tag <tag> et tout leurs fils
Si start est le noeud de tag <tag>, on retourne 1. si non on retourne toujours 0.
Le node est enleve de l'arbre mais pas supprime, il est retourne dans <deletedNode>*/
int tree_delete_node(Node * start, void * tag, Node ** deletedNode) {
	Node * target = tree_find_node(start, tag);
	if(target == NULL) {
		return 0;
	}

	if(start == target) {
		return 1;
	}

	Node * father = target->father;
	list_remove_data(&(father->sons), target);

	if(deletedNode != NULL) {
		*deletedNode = target;
	}

	father->sonsCount --;

	return 0;
}

/*Retourne une liste des nodes au dessus du node <node> et lui meme*/
List * tree_get_fathers(Node * node) {
	List * list = list_create_list(node);
	if(node->father == NULL) {
		return list;
	}
	for (Node * father = node->father; father != NULL; father = father->father) {
		list_add_data_end(list, father);
	}
	return list;
}

static void tree_get_descendants_on_node(Node * node, Node * father, List * list) {
	list_add_data_end(list, node);
}
/*Retourne une list des descendants du node*/
List * tree_get_descendants(Node * node) {
	List * list = list_create_list(node);
	tree_parcours_profondeur(node, (TreeFunction)tree_get_descendants_on_node, (void*)list);
	return list;
}


static void printline_indent(const char * str, int indent) {
	for (int i = 0; i < indent; i++) {
		printf("\t");
	}
	printf("%s\n", str);
}
static void tree_display_arbre_on_node(Node * node, Node * father, int profondeur, void * other) {
	printline_indent(node->tag, profondeur);
}
/*Affiche l'arbre <arbre>*/
void tree_display_arbre(Node * arbre) {
	printf("%s\n", arbre->tag);
	tree_parcours_profondeur_with_profondeur(arbre, (TreeFunctionProfondeur)tree_display_arbre_on_node, NULL);
}


static void tree_parcours_profondeur_with_profondeur_aux_on_fils(Node * f, void * allargs) {
	// On a un fils
	// Depaquettage du paquet
	void ** arg = (void**)allargs;
	TreeFunctionProfondeur callback = (TreeFunctionProfondeur) arg[0];
	Node * father = (Node*) arg[1];
	void * other = arg[2];
	int * indent = arg[3];
	Node ** prevPere = arg[4];
	
	// Petit warning pour signaler une incoherance dans l'arbre
	// (Petit truc de parano)
	if(father != NULL && father != f->father) {
		printf("Probleme dans l'arbre : Le fils ne connais pas son vrai father et son father le connais");
	}

	if(*prevPere == father->father) {
		// On viens de passer au premier fils du father
		// On choisi une indentation qui sera prise en compte par les fils du node
		(*indent)++;
	}
	int currentIndent = *indent;

	// On met a jour le father precedent
	*prevPere = f->father;

	
	// Appel du callback
	callback(f, father, currentIndent, other);

	// Maintenant le fils devient un father, et on continu
	tree_parcours_profondeur_with_profondeur_aux(f, callback, indent, prevPere, other);

	// On a fini avec les fils, on remet l'indentation du niveau
	*indent = currentIndent;
}
/*Fait un parcours en profondeur de l'arborescence de start.
pour chaque node, callback est appelle avec le node et son father*/
void tree_parcours_profondeur_with_profondeur_aux(Node * start, TreeFunctionProfondeur callback, int * indent, Node ** prevPere, void * other) {
	// Empacketage du callback et du father pour le parcours des fils de ce dernier
	void * arg[] = {callback, start, other, indent, prevPere};
	list_browse(start->sons, (ListFunction)tree_parcours_profondeur_with_profondeur_aux_on_fils, (void*)arg);
}
void tree_parcours_profondeur_with_profondeur(Node * start, TreeFunctionProfondeur callback, void * other) {
	Node * prevPere = NULL;
	int indent = 0;
	tree_parcours_profondeur_with_profondeur_aux(start, (TreeFunctionProfondeur) callback, &indent, &prevPere, other);
}

static void tree_node_list_to_tag_list_on_node(Node * node, List ** tags) {
	if(*tags == NULL) {
		*tags = list_create_list((void*)node->tag);
	}
	else {
		list_add_data_end(*tags, (void*)node->tag);
	}
}
/*Transforme une List de Node en une liste de char correspondant aux tags des Node*/
List * tree_node_list_to_tag_list(List * nodes) {
	List * tags = NULL;
	list_browse(nodes, (ListFunction)tree_node_list_to_tag_list_on_node, (void*)&tags);
	return tags;
}