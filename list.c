#include "list.h"

/* Creation d'une liste contenant un seul element*/
List * list_create_list(void * data) {
	List res;
	res.data = data;
	res.next = NULL;
	List * r = malloc(sizeof(List));
	*r = res;
	return r;
}

static void list_copy_on_element(List * element, void ** arg) {
	List ** new = (List **) arg[0];
	ListFunctionCopy copy = (ListFunctionCopy)arg[1];

	void * dataCopy = copy(element->data);

	if(*new == NULL) {
		*new = list_create_list(dataCopy);
	}
	else {
		list_add_data_end(*new, dataCopy);
	}
}
/*Fait une copy de la List*/
List * list_copy(List * list, ListFunctionCopy copy) {
	List * new = NULL;
	void * arg[] = {&new, copy};
	list_browse(list, (ListFunction)list_copy_on_element, arg);

	return new;
}

/* Detruit tous les elements de la liste
Ne detruit pas les data*/
void list_destroy(List * list) {
	while(list != NULL) {
		List * aux = list;
		list = list->next;
		free(aux);
	}
}

/* Detruit tous les elements de la liste
Detruit aussi les data*/
void list_destroy_free_data(List * list) {
	while(list != NULL) {
		List * aux = list;
		list = list->next;
		free(aux->data);
		free(aux);
	}
}

/* Detruit tous les elements de la liste en utilisant <destroyFunction> pour detruire les data*/
void list_destroy_with_function(List * list, ListFunctionDestroy destroyFunction) {
	while(list != NULL) {
		List * aux = list;
		list = list->next;
		destroyFunction(aux->data);
		free(aux);
	}
}

/* Ajout d'un element a une liste*/
void list_add_data_end(List * list, void * data) {
	List * current = NULL;
	for (current = list; current->next != NULL ; current = current->next) {}
	// He Op on est a la fin de la liste
	List aux;
	aux.data = data;
	aux.next = NULL;
	List * new = malloc(sizeof(struct List));
	*new = aux;
	current->next = new;
}

/*Suppression d'un element d'une liste.
L'element dont le pointeur data est le meme que le data passe en parametre est supprime
Si la liste ne contient qu'un element, elle est mise a NULL*/
void list_remove_data(List ** list, void * data) {
	List * current = NULL;
	List * previous = NULL;
	for (current=*list, previous=NULL ;current!=NULL ; previous=current, current=current->next) {
		if(current->data == data) {
			// On t'a trouve
			if(current->next == NULL) {
				if(previous == NULL) {
					// Le seul elemet de la list,
					*list = NULL;
				}
				else {
					// Le dernier element
					previous->next = NULL;
				}
			}
			else if(previous == NULL) {
				// Le premier element
				if(current->next == NULL) {
					// Le seul elemet de la list,
					// On ne fait rien
				}
				else {
					*list = current->next;
				}
			}
			else {
				// Un element intermediaire
				previous->next = current->next;
			}
			free(current);
			return;
		}
	}
}

/*Suppression d'un element d'une liste. Retourne 1 si quelque chose a ete supprime et 0 sinon
Suppression d'un element de la liste pour lequel when retourne TRUE
Si la liste ne contient qu'un element, elle est mise a NULL*/
int list_remove_data_when(List ** list, ListFunctionWhen when, void * other) {
	List * current = NULL;
	List * previous = NULL;
	for (current=*list, previous=NULL ;current!=NULL ; previous=current, current=current->next) {
		if(when(current->data, other)) {
			// On t'a trouve
			if(current->next == NULL) {
				if(previous == NULL) {
					// Le seul elemet de la list,
					*list = NULL;
				}
				else {
					// Le dernier element
					previous->next = NULL;
				}
			}
			else if(previous == NULL) {
				// Le premier element
				if(current->next == NULL) {
					// Le seul elemet de la list,
					*list = NULL;
				}
				else {
					*list = current->next;
				}
			}
			else {
				// Un element intermediaire
				previous->next = current->next;
			}
			free(current);
			return 1;
		}
	}
	return 0;
}

/* Parcours d'une liste*/
void list_browse(List * list, ListFunction callBack, void * other) {
	List * next = NULL;
	for(List * current = list; current != NULL ; current = next) {
		next = current->next;
		callBack(current->data, other);
	}
}

/* Retourne le premier retour de callBack qui n'est pas NULL
callBack est appeller avec chaque element de la liste jusqu'a ce qu'il retourne une valeur non NULL*/
void * list_browse_return(List * list, ListFunctionCheck callBack, void * other) {
	for (List * current = list; current != NULL ; current = current->next) {
		void * res = callBack(current->data, other);
		if(res != NULL) {
			return res;
		}
	}
	return NULL;
}