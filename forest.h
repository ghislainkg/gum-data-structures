#ifndef _GUM_FOREST_
#define _GUM_FOREST_

#include "list.h"
#include "node.h"

typedef struct Forest {
	List * trees; // Liste de Node racines
} Forest;

typedef void (*ForestFunction)(Node *tree, Node * node, Node * pere, void * other);

/*Cree une forest contenant un seul tree (une seule racine) */
Forest * forest_create(Node * tree);

/*Libere toute la memoire alouer pour la Forest*/
void forest_destroy(Forest * forest);

/*Ajoute un nouvel tree a la forest*/
void forest_add_tree(Forest * forest, Node * tree);

/*Ajoute un node (node) comme fils d'un autre (pere).
Si pere n'est pas dans la la forest ou est null, le node est ajouter comme tree*/
void forest_add_node(Forest * forest, Node * node, Node * pere);

/*Supprime un tree de la forest*/
void forest_remove_tree(Forest * forest, Node * tree);

/*Supprime un node de la forest*/
void forest_delete_tag(Forest * forest, Node * node);

/*Parcours la forest tree par tree.
Pour chaque node de chaque tree, callback est appelle avec ce node, son tree correspondant et la donnee other*/
void forest_browse(Forest * forest, ForestFunction callback, void * other);

/*Teste si un tag est deja dans une forest*/
Node * forest_is_tag_in(Forest * forest, void * tag);

#endif